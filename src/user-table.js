$(document).ready( function () {
    $('#smi-table').DataTable( {
        

        ajax: {
            url: 'http://localhost:3000/data',
        dataSrc: ''
    },
    columns: [
        { data: 'id', className: "id" },
        { data: 'RagioneSociale', className: "regione"},
        { data: 'Indirizzo', className: "indirizzo"},
        { data: 'Citta', className: "citta"},
        { data: 'Prov', className: "prov"},
        { data: 'CAP', className: "cap"},
        { data: 'PIVA', className: "piva"},
        { data: 'CF', className: "cf"},
        { data: 'Telefono', className: "tel"},
        { data: 'Fax', className: "fax"},
        { data: 'Email', className: "email"},
    ],
    columnDefs:[
        {
            render: function(){
                return "<button class='btn-delete btn btn-outline-success'> delete </button>";
            },
            targets: 12,
            
        }, {
            render: function(){
                return "<button class='btn-edit btn btn-outline-success'> edit </button>";
            },
            targets: 11,
        }
    ]
    });
    $("#smi-table_length").append('<button class="btn-new btn btn-outline-primary" id="btn-new">new</button>')

    let idRow = 0;

    let newData = function() {
        $('#smi-table').find('tr:eq(1)').before(`<tr class="new-row-data">
        <td><div>${idRow}</div></td>,
        <td><input type="text" id="regione"></td>
        <td><input type="text" id="indirizzo"></td>
        <td><input type="text" id="citta"></td>
        <td><input type="text" id="prov"></td>
        <td><input type="text" id="cap"></td>
        <td><input type="text" id="piva"></td>
        <td><input type="text" id="cf"></td>
        <td><input type="text" id="tel"></td>
        <td><input type="text" id="fax"></td>
        <td><input type="text" id="email"></td>
        <td><button class='btn-save btn btn-outline-success' id="btn-save"> save </button></td>
        <td><button class='btn-back btn btn-outline-success' id="btn-back"> delete </button></td>
        </tr>`);  
        $('#btn-new').hide();
    }  
    
    // $("#smi-table tr").on('click', function() {
    //     idRow++;
    //     newData();
    // });

    // $('#btn-back').on('click', function() {
    //     console.log('blblbl');
    //     $('.btn-new').show();
    //     idRow--;
    // });
    
    $('#smi-table').on('click', '.btn-back', function() {
        $('.btn-new').show();
        $('.new-row-data').remove();
        idRow--;
    });

    $('#btn-new').on('click', function() {
        idRow++;
        newData();
    });

    $('#smi-table').on('click', '.btn-save', function() {
        $('#smi-table').find('tr:eq(1)').before(`<tr class="new-row">
        <td class="id">${idRow}</td>,
        <td class="regione"></td>
        <td class="indirizzo"></td>
        <td class="citta"></td>
        <td class="prov"></td>
        <td class="cap"></td>
        <td class="piva"></td>
        <td class="cf"></td>
        <td class="tel"></td>
        <td class="fax"></td>
        <td class="email"></td>
        <td><button class='btn-edit btn btn-outline-success'> edit </button></td>
        <td><button class='btn-delete btn btn-outline-success'> delete </button></td>
        </tr>`)

        handleChange('.new-row');
        if(idRow % 2 === 0) {
            $('.new-row').addClass('odd');
            $('.new-row > .id').addClass('sorting_1')
        } else {
            $('.new-row').addClass('even');
        }
        $('.new-row').removeClass('new-row');
        $('.new-row-data').remove();
        $('.btn-new').show();
        
    });

});

$('#smi-table').on( 'click', '.btn-delete', function () {
    $(this).closest('tr').next('.changes').remove();
    $(this).closest('tr').remove();
} );

$('#smi-table').on('click', '.btn-edit', function () {
    $('.selected').removeClass('selected')
    $('.btn-edit').show();
    $('.changes').remove();
    let update = $(this)
    let handleEdit = function (position) {
        console.log(update.closest('tr').find(position).html())
        return update.closest('tr').find(position).html();
    }
    $(this).closest('tr').after(`<tr class="changes">
        <form>
            <td></td>
            <td><input type="text" value="${handleEdit('td:eq(1)')}" id="regione"></td>
            <td><input type="text" value="${handleEdit('td:eq(2)')}" id="indirizzo"></td>
            <td><input type="text" value="${handleEdit('td:eq(3)')}" id="citta"></td>
            <td><input type="text" value="${handleEdit('td:eq(4)')}" id="prov"></td>
            <td><input type="text" value="${handleEdit('td:eq(5)')}" id="cap"></td>
            <td><input type="text" value="${handleEdit('td:eq(6)')}" id="piva"></td>
            <td><input type="text" value="${handleEdit('td:eq(7)')}" id="cf"></td>
            <td><input type="text" value="${handleEdit('td:eq(8)')}" id="tel"></td>
            <td><input type="text" value="${handleEdit('td:eq(9)')}" id="fax"></td>
            <td><input type="text" value="${handleEdit('td:eq(10)')}" id="email"></td>
            <td><button class="btn-update btn btn-outline-success">update</button></td>
        </form>
    </tr>`).addClass('selected');
    $(this).hide();
});

$('#smi-table').on('click', '.btn-update', function () {
    $('.btn-edit').show();
    handleChange('.selected');
    $('.selected').removeClass('selected');
    $(this).closest('tr').remove();
});

let handleChange = function(classSelector) {
    let rep = $(classSelector)
    rep.find('.regione').text($('#regione').val())
    rep.find('.indirizzo').text($('#indirizzo').val());
    rep.find('.citta').text($('#citta').val());
    rep.find('.prov').text($('#prov').val());
    rep.find('.cap').text($('#cap').val());
    rep.find('.piva').text($('#piva').val());
    rep.find('.cf').text($('#cf').val());
    rep.find('.tel').text($('#tel').val());
    rep.find('.email').text($('#email').val()); 
}